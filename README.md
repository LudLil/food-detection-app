## whatFOOD is an experimental app with camera used for image recognition

* Camera should take picture and store them locally
* Image in storage should be processed by ML-Kit recognition algorithms - suggest using food items
* Test augmented reality on live capturing of food through the camera (Open CV?)

## Current status: 21st of April
* Preliminary UI for handling camera and images is in place
* The detectFood function on one single image is tested and working on our test project Delicious.
* Functions for object recognition for live images and a single image is finished but not tested and UI is not linked (only the image labeling via detectFood is connected to UI)
* A room-database for images is set up, and should work
* Clicking on an image retrieved from room, that is not the app icon or button images, in PhotosActivity, does not open PhotoInfoActivity, but it works when the images are retrieved from URLs instead. I assume the app icon and the button images that are added to and retrieved from room are small enough to where their bitmaps can be passed with an intent, making PhotoInfoActivity open
* The camera function is working on our test project Delicious and are ported to the app (trackfood)
* Augmented overlay for live camera or pictures is not implemented (only ARCore settings are in place)
* UI is working in portrait and landscape mode


## Participants
* vanjako  | Vanja Falck       - vanja.falck@ntnu.no and vanja.falck@gmail.com
* viktorjp | Viktor Palmason   - viktorjp@stud.ntnu.no and viktorjarlpalmason@gmail.com
* ludvigli | Ludvig Lilleberg  - ludvigli@stud.ntnu.no
* Mg       | Marion Gapaillard - akyaetlop@gmail.com

## Functionality
* Recognize a food item from an image (and maybe only by pointing the camera to an item?)
* Put augmented reality over the food items in camera (not yet implemented)
* Show where a food item is grown or how its origin plant look like (not yet implemented)

## Structure of app (fill in)
* MainActivity coupled with activity_main.xml given 'id: main_activity'
* Extensions.kt : use this for 'global constants and global functions' - functions for object recognition and image labeling is here.
* ImageAnalyzer: class for analysing images transcoded to FirebaseVisionImage
* Firebase and image-label and object-detection is already set up in manifest and gradle files
* ML-Kit image labeling
* Gradle settings for CameraX is in place
* Using Android Studio 3.6.2
* Using Kotlin 1.3.72
* ARCore (only settings added)

### MainActivity

| id | Description |
| ------ | ------ |
| `main_activity` | Main top-level layout `id` |
| `imgrec_btn` | Button that goes to the image recognition activity |
| `ar_btn` | Button that goes to the AR activity |
| `settings_btn` | Button that goes to the settings activity |

### SettingsActivity

| id | Description |
| ------ | ------ |
| `settings_activity` | Settings top-level layout `id` |
| `settings_container` | FragmentContainerView that holds SettingsFragment |
| `settings_back_btn` | Button that goes back to MainActivity |

### SettingsFragment

* Uses preferences.xml in the xml directory to set preferences

### ImgRecActivity

| id | Description |
| ------ | ------ |
| `imgrec_activity` | ImgRec top-level layout `id` |
| `take_photo_btn` | Button that starts photo capture mode and goes to CameraActivity |
| `view_photos_btn` | Button that goes to PhotosActivity |
| `imgrec_back_btn` | Button that goes back to MainActivity |

### CameraActivity

| id | Description |
| ------ | ------ |
| `camera_activity` | Camera top-level layout `id` |
| `new_photo_btn` | Button that starts photo capture mode |
| `camera_back_btn` | Button that goes back to ImgRecActivity |

### PhotosActivity

| id | Description |
| ------ | ------ |
| `photos_activity` | Photos top-level layout `id` |
| `photo_display` | RecyclerView that uses the class PhotoGridAdapter to display photos stored in Room DB in a grid |
| `photos_back_btn` | Button that goes back to ImgRecActivity |

### PhotoInfoActivity

| id | Description |
| ------ | ------ |
| `photoinfo_activity` | Main top-level layout `id` |
| `analyzed_photo` | ImageView that contains the image to be analyzed |
| `has_food_text` | TextView that contains text saying whether a food item could be detected in the image |
| `label_display` | RecyclerView that uses the class LabelListAdapter to display a list of all labels that are found when analyzing the image |
| `photoinfo_back_btn` | Button that goes back to the previous activity |

## TODO
* Use info at: "https://firebase.google.com/docs/ml-kit/android/label-images"
* Change the current camera code to use the CameraX library (that allows rotation of image):
* We need to have a live image recognition:
1) Prepare the input image
* Getting the labels:
2) Configure and run the image labeler
* Get info about labeled objects:
3) Get information about labeled objects
* To detect and track objects:
* Use info at: "https://firebase.google.com/docs/ml-kit/android/detect-objects"
* Set up tests for UI
* Set up tests for functionality
* Set up ARCore for augmentetd reality (files are in ARCore_example in this repo, possible help form this tutorial): "https://www.raywenderlich.com/6986535-arcore-with-kotlin-getting-started")

## Task responsibilities (please update)
* Image labeling and object recognition: Vanja
* UI: Ludvig
* Setting up camera and CameraX library: Viktor
* Optional to explore: ARCore - openCV - augmented effects

## Development resources
* Enclosed DeliciousFood-App in this repo: the app is using the ML-Kit on-device and cloud based services (two different functions) to label images - it only returns if the image is a "delicious food" or not, but is instructive!
* ML Kit Machine Learning intro (from 7:34) - code (from 21:02 ) - live detection (from 22:10) "https://www.youtube.com/watch?v=Z-dqGRSsaBs"
* Tensorflow lite to make Android models for image recognition:"https://colab.research.google.com/github/tensorflow/examples/blob/master/tensorflow_examples/lite/model_maker/demo/image_classification.ipynb#scrollTo=pyju1qc_v-wy"
* Food recognition dataset Food-101 (101.000 images complete +5GB can be downloaded from here): https://data.vision.ee.ethz.ch/cvl/datasets_extra/food-101/
* TensorFlow access to Food-101 (101.000 images) "https://www.tensorflow.org/datasets/catalog/food101"
* Practical code guides for image recognition ml kit (kotlin): "https://github.com/firebase/quickstart-android/blob/master/mlkit/app/src/main/java/com/google/firebase/samples/apps/mlkit/kotlin/StillImageActivity.kt"
* Tutorial live recognition through frames: "https://heartbeat.fritz.ai/building-a-real-time-object-detection-app-on-android-using-firebase-ml-kit-c72c99daf46f"
* Displaying images in a grid: "https://acomputerengineer.com/2019/05/09/display-image-grid-in-recyclerview-in-kotlin-android/"
* Saving images in Room: "https://medium.com/@uttam.cooch/save-images-in-room-persistence-library-c71b60865b7e"

## Testing resources
* Kotlin/UI-test with Espresso (tutorial): "https://www.youtube.com/watch?v=60KFJTb_HwU
"
* Best practice kotlin/testing (2018): "https://www.youtube.com/watch?v=RX_g65J14H0"
* Android general about testing: "https://developer.android.com/training/testing/fundamentals"
* Androids tutorial on UI testing: "https://developer.android.com/training/testing/ui-testing/espresso-testing"

## Firebase references of importance for this project
* FirebaseVisionLabelDetector (important class for any object recognition through use of labeled images): "https://firebase.google.com/docs/reference/android/com/google/firebase/ml/vision/label/FirebaseVisionLabelDetector"
* FirebaseVisionLabel (a list of this object is returned from FirebaseVisionLabelDetector): "https://firebase.google.com/docs/reference/android/com/google/firebase/ml/vision/label/FirebaseVisionLabel"

## Image recognition algorithms - with own models
* Convert tensorflow/keras models to tensorflow lite (mobile) from the TensorFlow Lite guide:
* With python api: "https://www.tensorflow.org/lite/convert/python_api"
* Command line api with tf-nightly: "https://www.tensorflow.org/lite/convert/cmdline"
* Optimize by quantizing (option in conversion): "https://www.tensorflow.org/lite/convert/quantization"
