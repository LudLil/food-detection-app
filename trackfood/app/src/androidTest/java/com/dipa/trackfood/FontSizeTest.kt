package com.dipa.trackfood

import android.content.res.Resources
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.BoundedMatcher
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import org.hamcrest.CoreMatchers.anything
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

fun withFontSize(expectedSize: Int): Matcher<View> {
    return object : BoundedMatcher<View, View>(View::class.java) {

        public override fun matchesSafely(view: View): Boolean {
            if (view !is TextView) {
                return false
            }
            val px = view.textSize
            val sp = (px / Resources.getSystem().displayMetrics.scaledDensity).toInt()

            return sp == expectedSize
        }

        override fun describeTo(description: Description) {
            description.appendText("with fontSize: ")
            description.appendValue(expectedSize)
        }
    }
}

@RunWith(AndroidJUnit4::class)
@LargeTest
class FontSizeTest {

    @get:Rule
    val intentsRule = IntentsTestRule(MainActivity::class.java)

    private fun changeFontSize(listItem: Int) {
        onView(withId(R.id.main_activity)).check(matches(isDisplayed()))
        onView(withId(R.id.settings_btn)).perform(click())
        onView(withId(R.id.settings_activity)).check(matches(isDisplayed()))

        onView(withId(R.id.recycler_view))
            .perform(
                RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                    0, click()
                )
            )

        onData(anything()).inAdapterView(withId(R.id.select_dialog_listview))
            .atPosition(listItem).perform(click())

        onView(withId(R.id.settings_back_btn)).perform(click())
        onView(withId(R.id.main_activity)).check(matches(isDisplayed()))
    }

    private fun checkMainFontSize(size: Int) {
        onView(withId(R.id.imgrec_btn)).check(matches(withFontSize(size)))
        onView(withId(R.id.ar_btn)).check(matches(withFontSize(size)))
        onView(withId(R.id.imgrec_btn)).perform(click())
        onView(withId(R.id.imgrec_activity)).check(matches(isDisplayed()))
    }

    private fun checkImgRecFontSize(size: Int) {
        onView(withId(R.id.take_photo_btn)).check(matches(withFontSize(size)))
        onView(withId(R.id.view_photos_btn)).check(matches(withFontSize(size)))
        onView(withId(R.id.imgrec_back_btn)).check(matches(withFontSize(size)))
        onView(withId(R.id.take_photo_btn)).perform(click())
        onView(withId(R.id.camera_activity)).check(matches(isDisplayed()))
    }

    private fun checkCameraFontSize(size: Int) {
        onView(withId(R.id.new_photo_btn)).check(matches(withFontSize(size)))
        onView(withId(R.id.camera_back_btn)).check(matches(withFontSize(size)))
        onView(withId(R.id.camera_back_btn)).perform(click())
        onView(withId(R.id.view_photos_btn)).perform(click())
        onView(withId(R.id.photos_activity)).check(matches(isDisplayed()))
    }

    private fun checkPhotosFontSize(size: Int) {
        onView(withId(R.id.photos_back_btn)).check(matches(withFontSize(size)))
        onView(withId(R.id.photo_display))
            .perform(
                RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                    0, click()
                )
            )
        onView(withId(R.id.photoinfo_activity)).check(matches(isDisplayed()))
        onView(withId(R.id.delete_img_btn)).check(matches(withFontSize(size)))
        onView(withId(R.id.photoinfo_back_btn)).check(matches(withFontSize(size)))
        onView(withId(R.id.photoinfo_back_btn)).perform(click())
        onView(withId(R.id.photos_back_btn)).perform(click())
        onView(withId(R.id.imgrec_back_btn)).perform(click())
    }

    @Test
    fun fontSizeTest() {
        var size = 14
        for (i in 0..2) {
            when(i) {
                0 -> size = 14
                1 -> size = 17
                2 -> size = 20
            }
            changeFontSize(i)
            checkMainFontSize(size)
            checkImgRecFontSize(size)
            checkCameraFontSize(size)
            checkPhotosFontSize(size)
        }
        changeFontSize(1)
    }


}