package com.dipa.trackfood

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.photo_grid_cell.view.*

// Adapter for grid RecyclerView in PhotosActivity
class PhotoGridAdapter(private val context: Context, private val images: List<Image/*Bitmap String*/>) :
    RecyclerView.Adapter<PhotoGridAdapter.ColorViewHolder>() {

    override fun getItemCount(): Int {
        return images.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ColorViewHolder {
        return ColorViewHolder(LayoutInflater.from(context).inflate(R.layout.photo_grid_cell, parent, false))
    }

    // Insert images into grid
    // When user clicks on an image, the image is analyzed in PhotoInfoActivity
    override fun onBindViewHolder(holder: ColorViewHolder, position: Int) {
        val image = images[position]

        // Set bitmap of DB image to ImageView
        val imageID = image.imgID
        val imgByteArray = image.imgByteArray
        val bitmap = BitmapFactory.decodeByteArray(imgByteArray, 0, imgByteArray.size)
        picassoLoadBitmap(context, bitmap, holder.imgView)

        // Send ID of image to PhotoInfoActivity
        holder.imgView.setOnClickListener {
            val intent = Intent(context, PhotoInfoActivity::class.java)
            intent.putExtra("image_id", imageID)
            (context as Activity).startActivityForResult(intent, 1)
        }
    }

    class ColorViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val imgView = view.imgView as ImageView
    }
}