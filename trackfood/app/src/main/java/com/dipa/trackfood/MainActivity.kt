package com.dipa.trackfood

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.preference.PreferenceManager.*
import com.google.firebase.FirebaseApp
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Sets button text size according to settings
        val sharedPreferences = getDefaultSharedPreferences(this)
        val fontSize = sharedPreferences.getString("fontSize", "14")?.toFloat()
        if (fontSize != null) {
            val spCode = 2
            imgrec_btn.setTextSize(spCode, fontSize)
            ar_btn.setTextSize(spCode, fontSize)
            settings_btn.setTextSize(spCode, fontSize)
        }

        FirebaseApp.initializeApp(this)

        imgrec_btn.setOnClickListener {
            val intent = Intent(this, ImgRecActivity::class.java)
            startActivity(intent)
        }

        ar_btn.setOnClickListener {
            val toast = Toast.makeText(this, "To be implemented", Toast.LENGTH_SHORT)
            toast.show()
        }

        settings_btn.setOnClickListener {
            val intent = Intent(this, SettingsActivity::class.java)
            startActivity(intent)
        }
    }
}
