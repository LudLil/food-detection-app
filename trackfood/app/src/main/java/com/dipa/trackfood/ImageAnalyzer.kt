package com.dipa.trackfood

// UNDER DEVELOPMENT _ NOT USED
/*
import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.ImageFormat
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageProxy
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.common.FirebaseVisionImageMetadata
import java.nio.ByteBuffer*/

// TODO: make sure the CameraX library is used to handle images on the device (otherwise we have to manually adjust rotation)

/*
----
Notice on AUGMENTATION of text, graphics etc over FirebaseVisionImages
Info at: https://firebase.google.com/docs/ml-kit/android/label-images
If you are using the output of the image labeler to overlay graphics on
the input image, first get the result from ML Kit, then render the image
and overlay in a single step. By doing so, you render to the display
surface only once for each input frame. See the CameraSourcePreview and
GraphicOverlay classes in the quickstart sample app for an example.
----

Code depends on implementing CameraX libraries to take care of rotation of images.
Creating FirebaseVisionImage depend upon giving rotation data (even though that is not so
important for food items).

MediaImages (from storage on device):
Requires use of android.hardware.camera2 API and image format JPEG/YUF_420_888!!!

ImageAnalyzer() transcode images from different formats into
FirebaseVisionImages that are used as input for image/object recognition
in ML Kit

*/

    /*class ImageAnalyzer : ImageAnalysis.Analyzer {

        // Settings for image size that is ok for recognition of food items
        var degree:Int = 0
        val METADATA = FirebaseVisionImageMetadata.Builder()
            .setWidth(480)
            .setHeight(360)
            .setFormat(FirebaseVisionImageMetadata.IMAGE_FORMAT_NV21)
            .setRotation(degreesToFirebaseRotation(degree))
            .build()
        // If using Camera2 API for capture image
        val camera2ImageFormat = ImageFormat.YUV_420_888
        // If using older Camera API for capture image
        val cameraImageFormat = ImageFormat.NV21

        private fun degreesToFirebaseRotation(degrees: Int): Int = when(degrees) {
            0 -> FirebaseVisionImageMetadata.ROTATION_0
            90 -> FirebaseVisionImageMetadata.ROTATION_90
            180 -> FirebaseVisionImageMetadata.ROTATION_180
            270 -> FirebaseVisionImageMetadata.ROTATION_270
            else -> throw Exception("Rotation must be 0, 90, 180, or 270.")
        }

        /**

        PREPARING FirebaseVisionImages from different sources

        @SuppressLint("UnsafeExperimentalUsageError")
        = Experimental use of imageProxy?.image for converting MediaImages

        MediaImage .... is use for images stored on the device
        Bitmap ........ is used for the camera capture

         */

        // FAST (DEPENDS ON CAMERAX)
        @SuppressLint("UnsafeExperimentalUsageError")
        fun analyzeMediaImage(imageProxy: ImageProxy?, degrees: Int = 0): FirebaseVisionImage {
            val mediaImage = imageProxy?.image
            val imageRotation = degreesToFirebaseRotation(degrees)
            //if (mediaImage != null) {
                // Using MediaImages (from storage on device) requires use of android.hardware.camera2 API
                // and the JPEG/YUF_420_888 image format to work!!!
                val image = FirebaseVisionImage.fromMediaImage(mediaImage!!, imageRotation)
                // Pass image to an ML Kit Vision API
                // TODO: make a separate function
                 return image
             //}
        }

        // SLOWER
        fun analyzeByteBuffer(buffer:ByteBuffer, metadata:FirebaseVisionImageMetadata = METADATA): FirebaseVisionImage{
            val image = FirebaseVisionImage.fromByteBuffer(buffer,metadata)
            // Pass image to an ML Kit Vision API
            // TODO: make a separate function
            return image
        }

        // SLOWER
        fun analyzeByteArray(array: ByteArray, metadata:FirebaseVisionImageMetadata = METADATA): FirebaseVisionImage{
            val image = FirebaseVisionImage.fromByteArray(array,metadata)
            // Pass image to an ML Kit Vision API
            // TODO: make a separate function
            return image
        }

        // FAST
        fun analyzeBitmap(bitmapImage: Bitmap): FirebaseVisionImage {
            val image = FirebaseVisionImage.fromBitmap(bitmapImage)
            // Pass image to an ML Kit Vision API
            // TODO: make a separate function
            return image
        }

        override fun analyze(image: ImageProxy?, rotationDegrees: Int) {
            analyzeMediaImage(image)
        }
    }*/

fun main() {

       // Test here

}