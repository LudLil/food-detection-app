package com.dipa.trackfood

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.preference.PreferenceManager.getDefaultSharedPreferences
import com.google.android.material.snackbar.Snackbar
import com.mindorks.paracamera.Camera
import androidx.core.graphics.drawable.toBitmap
import kotlinx.android.synthetic.main.activity_camera.*
import java.io.ByteArrayOutputStream

class CameraActivity : AppCompatActivity() {
    private val permissionRequestCode = 1
    private lateinit var camera: Camera

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_camera)

        // Sets button text size according to settings
        // Configure Camera
        camera = Camera.Builder()
            .resetToCorrectOrientation(true)//1
            .setTakePhotoRequestCode(Camera.REQUEST_TAKE_PHOTO)//2
            .setDirectory("pics")//3
            .setName("delicious_${System.currentTimeMillis()}")//3
            .setImageFormat(Camera.IMAGE_JPEG)//4
            .setCompression(75)//5
            .build(this)

        // Sets text size according to settings
        val sharedPreferences = getDefaultSharedPreferences(this)
        val fontSize = sharedPreferences.getString("fontSize", "14")?.toFloat()
        if (fontSize != null) {
            val spCode = 2
            new_photo_btn.setTextSize(spCode, fontSize)
            camera_back_btn.setTextSize(spCode, fontSize)
        }

        camera_back_btn.setOnClickListener {
            val intent = Intent(this, ImgRecActivity::class.java)
            startActivity(intent)
        }

        photoView.setOnLongClickListener {

            return@setOnLongClickListener true
        }

        new_photo_btn.setOnClickListener {
            takePicture()
        }
        takePicture()
    }

    private fun takePicture() {
        if (!hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) ||
            !hasPermission(Manifest.permission.CAMERA)
        ) {
            // If do not have permissions then request it
            requestPermissions()
        } else {
            // else all permissions granted, go ahead and take a picture using camera
            try {
                camera.takePicture()
            } catch (e: Exception) {
                // Show a toast for exception
                Toast.makeText(
                    this.applicationContext, getString(R.string.error_taking_picture),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }


    private fun requestPermissions() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(
                this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
        ) {

            photoView.snack(
                getString(R.string.permission_message),
                Snackbar.LENGTH_INDEFINITE
            ) {
                action(getString(R.string.OK)) {
                    ActivityCompat.requestPermissions(
                        this@CameraActivity,
                        arrayOf(
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA
                        ), permissionRequestCode
                    )
                }
            }
        } else {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA
                ), permissionRequestCode
            )
            return
        }
    }

    private fun hasPermission(permission: String): Boolean {
        return ActivityCompat.checkSelfPermission(
            this,
            permission
        ) == PackageManager.PERMISSION_GRANTED

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        when (requestCode) {
            permissionRequestCode -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty()
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED
                ) {
                    try {
                        camera.takePicture()
                    } catch (e: Exception) {
                        Toast.makeText(
                            this.applicationContext, getString(R.string.error_taking_picture),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
                return
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Camera.REQUEST_TAKE_PHOTO) {
                val bitmap = camera.cameraBitmap
                if (bitmap != null) {
                    picassoLoadBitmap(this, bitmap, photoView)

                    photoView.setOnLongClickListener {
                        if (photoView.drawable == null) {
                            Toast.makeText(this, "Image not exists", Toast.LENGTH_SHORT).show()
                            return@setOnLongClickListener false
                        } else {
                            val db = ImageRoomDB.getInstance(this)
                            val photoBitmap = photoView.drawable.toBitmap()

                            // Blob (Binary Large Object) used to compress bitmap
                            val blob = ByteArrayOutputStream()
                            photoBitmap.compress(Bitmap.CompressFormat.JPEG, 100, blob)

                            // Save the blob's byte array to Room
                            val imgByteArray = blob.toByteArray()
                            val image = Image(0 , imgByteArray)           // Table row with image byte array
                            val newKey = db.imageDao.insertImage(image).toInt() // Add image to database and set new primary key

                            val intent = Intent(this, PhotoInfoActivity::class.java)
                            intent.putExtra("image_id", newKey)
                            startActivityForResult(intent, 1)

                            return@setOnLongClickListener true
                        }
                    }

                } else {
                    Toast.makeText(
                        this.applicationContext, getString(R.string.picture_not_taken),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    private fun View.snack(message: String, length: Int = Snackbar.LENGTH_LONG, f: Snackbar.() -> Unit) {
        val snack = Snackbar.make(this, message, length)
        snack.f()
        snack.show()
    }

    private fun Snackbar.action(action: String, color: Int? = null, listener: (View) -> Unit) {
        setAction(action, listener)
        color?.let { setActionTextColor(color) }
    }
}
