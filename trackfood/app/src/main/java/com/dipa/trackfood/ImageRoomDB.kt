package com.dipa.trackfood

import android.content.Context
import androidx.room.*

// DB table and columns, all rows have unique byte arrays
@Entity(tableName = "image_table", indices = [(Index(value = ["imgByteArray"], unique = true))])
data class Image (
    @PrimaryKey(autoGenerate = true) val imgID: Int,
    @ColumnInfo(name = "imgByteArray", typeAffinity = ColumnInfo.BLOB) val imgByteArray: ByteArray) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Image

        if (!imgByteArray.contentEquals(other.imgByteArray)) return false

        return true
    }

    override fun hashCode(): Int {
        return imgByteArray.contentHashCode()
    }
}

@Dao
interface ImageDao {
    @Query("SELECT * FROM image_table")
    fun getAllImages(): List<Image>

    @Query("SELECT * FROM image_table WHERE imgID = :id LIMIT 1")
    fun getImageById(id: Int): Image

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertImage(image: Image): Long

    @Delete
    fun deleteImage(image: Image)

    @Query("DELETE FROM image_table")
    fun deleteAllImages()
}

@Database(entities=[Image::class], version = 9, exportSchema = false)
abstract class ImageRoomDB : RoomDatabase() {
    abstract val imageDao : ImageDao

    companion object {
        @Volatile
        private var INSTANCE: ImageRoomDB? = null
        fun getInstance(context: Context): ImageRoomDB {
            synchronized(this) {
                var instance = INSTANCE

                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        ImageRoomDB::class.java,
                        "image_database"
                    )
                        .fallbackToDestructiveMigration()
                        .allowMainThreadQueries()
                        .build()
                    INSTANCE = instance
                }
                return instance
            }
        }
    }
}