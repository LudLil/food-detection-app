package com.dipa.trackfood

// Add global functions and constants here

// Own tensorflow lite models image recognition:
// https://colab.research.google.com/github/tensorflow/examples/blob/master/tensorflow_examples/lite/model_maker/demo/image_classification.ipynb
// image classification reference project on android (and ios):
// https://github.com/tensorflow/examples/tree/master/lite/examples/image_classification

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Typeface
import android.net.Uri
import android.util.Log
import android.view.*
import android.widget.*
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.label.FirebaseVisionImageLabel
import com.google.firebase.ml.vision.label.FirebaseVisionOnDeviceImageLabelerOptions
import com.squareup.picasso.Picasso
import java.io.File
import java.io.OutputStream

fun picassoLoadBitmap(pContext: Context, pBitmap: Bitmap, pImageView: ImageView) {
    try {
        // Create URI of temporary file, compress bitmap into URI output stream,
        // use Picasso to load URI
        val uri: Uri =
            Uri.fromFile(File.createTempFile("temp_file_name", ".jpg", pContext.cacheDir))
        val outputStream: OutputStream? = pContext.contentResolver.openOutputStream(uri)
        pBitmap.compress(Bitmap.CompressFormat.JPEG, 50, outputStream)
        outputStream?.close()
        Picasso.get().load(uri).resize(500, 660).centerCrop().into(pImageView)
    } catch (e: Exception) {
        Log.e("LoadBitmapByPicasso", e.message!!)
    }
}

// Set up cameraX
// source: https://blog.mindorks.com/getting-started-with-camerax (bad)
// official code in kotlin: https://github.com/android/camera-samples/tree/master/CameraXBasic


/**
 *  detectFood() takes a bitmap image (directly from the camera) and make an on device
 *  image recognition which finds the guessed objects label info:
 *
 *  label text .......... naming the object the recognition algorithm was guessing
 *  label confidence .... the probability of the object being guessed
 *  label entityId ...... the guessed object reference Id
 *
 *  detectObject() takes a bitmap image or live stream from camera
 *  object recognition finds:
 *
 *  obj boundingBox ....................... a rectangle enclosing the object in the image
 *  obj classificationCategory (int) ...... an id for the category of object identified
 *  obj classificationConfidence (float) .. probability for assigning correct category
 *
 *  The following categories are supported:
 *  const int CATEGORY_HOME_GOOD (= 1)
 *  const int CATEGORY_FASHION_GOOD (= 2)
 *  const int CATEGORY_FOOD (= 3)
 *  const int CATEGORY_PLACE (= 4)
 *  const int CATEGORY_PLANT (= 5)
 *  const int CATEGORY_UNKNOWN (= 0)
 *
 */

fun detectFood(hasFoodTextView: TextView, labelDisplay: TableLayout, context: Context, bitmap: Bitmap) {

    val image = FirebaseVisionImage.fromBitmap(bitmap)
    val options = FirebaseVisionOnDeviceImageLabelerOptions.Builder()
        .setConfidenceThreshold(0.75f)
        .build()

    var hasFood = false

    // Info about functions: https://firebase.google.com/docs/ml-kit/android/label-images
    //val labeler = FirebaseVision.getInstance().onDeviceImageLabeler //getOnDeviceImageLabeler()
    val labeler = FirebaseVision.getInstance().getOnDeviceImageLabeler(options)
    labeler.processImage(image)
        .addOnSuccessListener { labels->
            for (label in labels) {
                if (label.text.contains("food", true)) hasFood = true
            }
            if (hasFood) {
                hasFoodTextView.text = context.resources.getString(R.string.food_detected)
            } else {
                hasFoodTextView.text = context.resources.getString(R.string.no_food_detected)
            }
        }

        .addOnFailureListener {
            val toast = Toast.makeText(context, "Error analyzing image", Toast.LENGTH_LONG)
            toast.show()
        }

        .addOnCompleteListener { labels ->
            if (labels.result != null) {
                if (hasFood) {
                    val labelList = labels.result!!
                    updateTable(labelList, labelDisplay, context)
                }
            }
        }
}

// Update table in PhotoInfoActivity with labels
fun updateTable(labelList: MutableList<FirebaseVisionImageLabel>, labelDisplay: TableLayout, context: Context) {
    var row: TableRow
    var labelText: TextView
    var confidenceText: TextView

    // Header row
    row = TableRow(context)
    row.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)

    labelText = TextView(context)
    labelText.layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT, 1.toFloat())
    labelText.gravity = Gravity.CENTER
    labelText.setPadding(5, 5, 5, 5)
    labelText.text = context.resources.getString(R.string.found)
    labelText.textSize = 20.toFloat()
    labelText.setTypeface(null, Typeface.BOLD)
    row.addView(labelText)

    confidenceText = TextView(context)
    confidenceText.layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT, 1.toFloat())
    confidenceText.gravity = Gravity.CENTER
    confidenceText.setPadding(5, 5, 5, 5)
    confidenceText.text = context.resources.getString(R.string.confidence)
    confidenceText.textSize = 20.toFloat()
    confidenceText.setTypeface(null, Typeface.BOLD)
    row.addView(confidenceText)

    labelDisplay.addView(row)

    // Adds rows for all expenses if the list of expenses is not empty
    if (labelList.isNotEmpty()) {
        for (i in labelList) {
            row = TableRow(context)
            row.layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )

            labelText = TextView(context)
            labelText.layoutParams = TableRow.LayoutParams(
                TableRow.LayoutParams.WRAP_CONTENT,
                TableRow.LayoutParams.WRAP_CONTENT,
                1.toFloat()
            )
            labelText.gravity = Gravity.CENTER
            labelText.setPadding(5, 5, 5, 5)
            labelText.text = i.text
            labelText.textSize = 20.toFloat()
            row.addView(labelText)

            confidenceText = TextView(context)
            confidenceText.layoutParams = TableRow.LayoutParams(
                TableRow.LayoutParams.WRAP_CONTENT,
                TableRow.LayoutParams.WRAP_CONTENT,
                1.toFloat()
            )
            confidenceText.gravity = Gravity.CENTER
            confidenceText.setPadding(5, 5, 5, 5)
            val confidenceIntString = (i.confidence * 100).toInt().toString()
            val percentageString = context.resources.getString(R.string.percentage, confidenceIntString)
            confidenceText.text = percentageString
            confidenceText.textSize = 20.toFloat()
            row.addView(confidenceText)

            labelDisplay.addView(row)
        }
    }
}
/*

OBJECT RECOGNITION _ NOT CURRENTLY USED

// Detect multiple objects in single image
fun detectObjectSingleImage(bitmap:Bitmap) {
    val image = FirebaseVisionImage.fromBitmap(bitmap)
    val options = FirebaseVisionObjectDetectorOptions.Builder()
        .setDetectorMode(FirebaseVisionObjectDetectorOptions.SINGLE_IMAGE_MODE)
        .enableClassification()
        .enableMultipleObjects()
        .build()
    val objectDetector = FirebaseVision.getInstance().getOnDeviceObjectDetector(options)
    var hasFood = false
    objectDetector.processImage(image)
        .addOnSuccessListener { detectedObjects ->
            // Task completed successfully
            for (obj in detectedObjects) {
                val id =
                    obj.trackingId       // A number that identifies the object across images
                val bounds = obj.boundingBox
                // If classification was enabled:
                val category = obj.classificationCategory
                val confidence = obj.classificationConfidence
                if (category == 3) hasFood = true
            }
        }
        .addOnFailureListener { e ->
            // Task failed with an exception
            // Make a sane error message to UI
            // For testing only:
            println("Did not recognize any objects")
        }
}

// Detect a single object live
fun detectObjectLive(liveImage: FirebaseVisionImage){
    // transcode image to FirebaseVisionImage
    val options = FirebaseVisionObjectDetectorOptions.Builder()
        .setDetectorMode(FirebaseVisionObjectDetectorOptions.STREAM_MODE)
        .enableClassification()
        .enableMultipleObjects()
        .build()
    val objectDetector = FirebaseVision.getInstance().getOnDeviceObjectDetector(options)
    var hasFood = false
    objectDetector.processImage(liveImage)
        .addOnSuccessListener { detectedObjects ->
                // Task completed successfully
                for (obj in detectedObjects) {
                    val id = obj.trackingId       // A number that identifies the object across images
                    val bounds = obj.boundingBox
                    // If classification was enabled:
                    val category = obj.classificationCategory
                    //val confidence = obj.classificationConfidence
                    if (category == 3) hasFood = true
                }
            }
        .addOnFailureListener { e ->
            // Task failed with an exception
            // Make a sane error message to UI
            // For testing only:
            println("Did not recognize any objects")
        }
}

fun analyzeBitmap(bitmapImage: Bitmap) {
    val image = FirebaseVisionImage.fromBitmap(bitmapImage)
    // Pass image to an ML Kit Vision API (detectFood())
    //detectFood(image)
}*/

// CAMERA X
/*
/** Combination of all flags required to put activity into immersive mode */
const val FLAGS_FULLSCREEN =
    View.SYSTEM_UI_FLAG_LOW_PROFILE or
            View.SYSTEM_UI_FLAG_FULLSCREEN or
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
            View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or
            View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
            View.SYSTEM_UI_FLAG_HIDE_NAVIGATION

/** Milliseconds used for UI animations */
const val ANIMATION_FAST_MILLIS = 50L
const val ANIMATION_SLOW_MILLIS = 100L

/**
 * Simulate a button click, including a small delay while it is being pressed to trigger the
 * appropriate animations.
 */
fun ImageButton.simulateClick(delay: Long = ANIMATION_FAST_MILLIS) {
    performClick()
    isPressed = true
    invalidate()
    postDelayed({
        invalidate()
        isPressed = false
    }, delay)
}

/** Pad this view with the insets provided by the device cutout (i.e. notch) */
@RequiresApi(Build.VERSION_CODES.P)
fun View.padWithDisplayCutout() {

    /** Helper method that applies padding from cutout's safe insets */
    fun doPadding(cutout: DisplayCutout) = setPadding(
        cutout.safeInsetLeft,
        cutout.safeInsetTop,
        cutout.safeInsetRight,
        cutout.safeInsetBottom)

    // Apply padding using the display cutout designated "safe area"
    rootWindowInsets?.displayCutout?.let { doPadding(it) }

    // Set a listener for window insets since view.rootWindowInsets may not be ready yet
    setOnApplyWindowInsetsListener { _, insets ->
        insets.displayCutout?.let { doPadding(it) }
        insets
    }
}

/** Same as [AlertDialog.show] but setting immersive mode in the dialog's window */
fun AlertDialog.showImmersive() {
    // Set the dialog to not focusable
    window?.setFlags(
        WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
        WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE)

    // Make sure that the dialog's window is in full screen
    window?.decorView?.systemUiVisibility = FLAGS_FULLSCREEN

    // Show the dialog while still in immersive mode
    show()

    // Set the dialog to focusable again
    window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE)
}*/

// END CAMERA X


