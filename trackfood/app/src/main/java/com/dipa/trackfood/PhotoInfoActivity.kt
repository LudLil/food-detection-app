package com.dipa.trackfood

import android.graphics.BitmapFactory
import android.os.Bundle
import android.widget.TableLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceManager.*
import kotlinx.android.synthetic.main.activity_photo_info.*

class PhotoInfoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photo_info)

        // Sets button text size according to settings
        val sharedPreferences = getDefaultSharedPreferences(this)
        val fontSize = sharedPreferences.getString("fontSize", "14")?.toFloat()
        if (fontSize != null) {
            val spCode = 2
            photoinfo_back_btn.setTextSize(spCode, fontSize)
            delete_img_btn.setTextSize(spCode, fontSize)
        }

        // Get Room DB image from ID, get bitmap from image byte array,
        // load it into ImageView
        val imageID = intent.getIntExtra("image_id", 0)
        //Toast.makeText(this, "Image ID is $imageID", Toast.LENGTH_LONG).show()

        val db = ImageRoomDB.getInstance(this)
        val image = db.imageDao.getImageById(imageID)
        val imgByteArray = image.imgByteArray
        val bitmap = BitmapFactory.decodeByteArray(imgByteArray, 0, imgByteArray.size)

        picassoLoadBitmap(this, bitmap, analyzed_photo)

        // Sets has_food_text TexView text to has/does not have food,
        // updates table with label list
        detectFood(has_food_text, label_display as TableLayout,this, bitmap)

        delete_img_btn.setOnClickListener {
            // Delete image from DB, show toast, and go to previous activity
            db.imageDao.deleteImage(image)
            val toast = Toast.makeText(this, "Image deleted", Toast.LENGTH_SHORT)
            toast.show()
            finish()
        }

        // Goes back to previous activity
        photoinfo_back_btn.setOnClickListener {
            finish()
        }
    }
}