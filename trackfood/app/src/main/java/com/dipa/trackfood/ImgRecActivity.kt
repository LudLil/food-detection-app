package com.dipa.trackfood

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceManager.getDefaultSharedPreferences
import kotlinx.android.synthetic.main.activity_imgrec.*
import java.io.ByteArrayOutputStream

class ImgRecActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_imgrec)

        // Sets button text size according to settings
        val sharedPreferences = getDefaultSharedPreferences(this)
        val fontSize = sharedPreferences.getString("fontSize", "14")?.toFloat()
        if (fontSize != null) {
            val spCode = 2
            take_photo_btn.setTextSize(spCode, fontSize)
            view_photos_btn.setTextSize(spCode, fontSize)
            imgrec_back_btn.setTextSize(spCode, fontSize)
        }

        take_photo_btn.setOnClickListener {
            val intent = Intent(this, CameraActivity::class.java)
            startActivity(intent)
        }

        view_photos_btn.setOnClickListener {
            val db = ImageRoomDB.getInstance(this)
            // Get bitmap of image
            val bitmap = BitmapFactory.decodeResource(this.resources, R.drawable.burger)

            // Blob (Binary Large Object) used to compress bitmap
            val blob = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100 , blob)

            // Save the blob's byte array to Room
            val imgByteArray = blob.toByteArray()
            val image = Image(0, imgByteArray)  // Table row with image byte array
            db.imageDao.insertImage(image)            // Add image to DB

            val intent = Intent(this, PhotosActivity::class.java)
            startActivity(intent)
        }

        imgrec_back_btn.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }
}