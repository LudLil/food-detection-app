package com.dipa.trackfood

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceManager.*
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_photos.*

class PhotosActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photos)

        // Sets button text size according to settings
        val sharedPreferences = getDefaultSharedPreferences(this)
        val fontSize = sharedPreferences.getString("fontSize", "14")?.toFloat()
        if (fontSize != null) {
            val spCode = 2
            photos_back_btn.setTextSize(spCode, fontSize)
        }

        photo_display.layoutManager = GridLayoutManager(this, 3)
        val db = ImageRoomDB.getInstance(this)
        val imageList = db.imageDao.getAllImages()                // List of all DB images
        val photoGrid = PhotoGridAdapter(this, imageList)  // Display DB images in a grid
        photo_display.adapter = photoGrid

        photos_back_btn.setOnClickListener {
            val intent = Intent(this, ImgRecActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onResume() {
        super.onResume()
        // Updates photo grid after finish() is called in PhotoInfoActivity
        photo_display.layoutManager = GridLayoutManager(this, 3)
        val db = ImageRoomDB.getInstance(this)
        val imageList = db.imageDao.getAllImages()                // List of all DB images
        val photoGrid = PhotoGridAdapter(this, imageList)  // Display DB images in a grid
        photo_display.adapter = photoGrid
    }
}