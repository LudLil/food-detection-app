package com.dipa.trackfood

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.graphics.Matrix
import android.view.Surface
import android.view.TextureView

class CameraBuilder: AppCompatActivity() {

    private val requestCodePermissions = 10

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewFinder = findViewById(R.id.camera_activity)
        // Camera should show up in a TextureView

        // This is not working:
        //if (allPermissionsGranted()) {
            viewFinder.post { startCamera() }
        //}
        //else {
            //ActivityCompat.requestPermissions(this, REQUIRED_PERMISSION, requestCodePermissions)
        //}
        viewFinder.addOnLayoutChangeListener { _, _, _, _, _, _, _, _, _ ->
            updateTransform()
        }
    }

    //private val executor = Executors.newCachedThreadPool()
    private lateinit var viewFinder: TextureView

    private fun startCamera() {
        // Implement cameraX operations (from tutorial - NOT WORKING
        // TODO: find new way to start camera!!!
        // Create configuration object for the viewfinder use case

        /*
        val previewConfig = PreviewConfig.Builder().apply {
            setTargetResolution(Size(640, 480))
        }.build()


        // Build the viewfinder use case
        val preview = Preview(previewConfig)

        // Every time the viewfinder is updated, recompute layout
        preview.setOnPreviewOutputUpdateListener {

            // To update the SurfaceTexture, we have to remove it and re-add it
            val parent = viewFinder.parent as ViewGroup
            parent.removeView(viewFinder)
            parent.addView(viewFinder, 0)

            viewFinder.surfaceTexture = it.surfaceTexture
            updateTransform()
        }

        // Bind use cases to lifecycle
        // If Android Studio complains about "this" being not a LifecycleOwner
        // try rebuilding the project or updating the appcompat dependency to
        // version 1.1.0 or higher.
        CameraX.bindToLifecycle(this, preview)
        */

    }


    private fun updateTransform() {
        // Implement camera viewfinder transformations (from tutorial)
        val matrix = Matrix()

        // Compute the center of the view finder
        val centerX = viewFinder.width / 2f
        val centerY = viewFinder.height / 2f

        // Correct preview output to account for display rotation
        val rotationDegrees = when(viewFinder.display.rotation) {
            Surface.ROTATION_0 -> 0
            Surface.ROTATION_90 -> 90
            Surface.ROTATION_180 -> 180
            Surface.ROTATION_270 -> 270
            else -> return
        }
        matrix.postRotate(-rotationDegrees.toFloat(), centerX, centerY)

        // Finally, apply transformations to our TextureView
        viewFinder.setTransform(matrix)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == requestCodePermissions) {
            //if (allPermissionsGranted()) {
                viewFinder.post { startCamera() }
           // }
            //else {
                // Provide a reasonable error message to user
                //Toast.makeText((this@SomeContext, "Permission not granted by user", Toast.LENGTH_SHORT).show())
            //finish()
            //}
        }
    }


    // TODO: Find new way to handle camera permission - code in tutorial not working

    /*

    Followed a tutorial, but this function is not working

    private fun allPermissionsGranted() = requestCodePermissions.until(10) {
        ContextCompat.getMainExecutor(baseContext, it)
    }
    */

}

