package com.dipa.trackfood

//import android.content.res.Resources
//import android.graphics.BitmapFactory
import org.junit.Test

/**
 * Source of inspiration UI-test (Kotlin/JUnit5) relevant
 * - using mock() (=Espresso): https://www.youtube.com/watch?v=60KFJTb_HwU
 */

class ImageTest {

    /**
     * Testing if conversion from relevant image formats to
     * bitmap is done and correct
     */
    @Test
    fun convertToBitmapTest() {
        // Do all functions return a bitmap
    }

    /**
     * Testing if conversion from any image format to
     * FirebaseVisionImage is done and correct
     */
    @Test
    fun convertToFirebaseVisionImageTest() {
        // Do all function return FirebaseVisionImage
    }

    /**
     * Testing the image recognition function detectFood()
     * If known bitmap image in give the expected results
     * in label.text, label.confidence, (label.id?)
     */
    @Test
    fun detectFoodTest() {
        // Get testimage from url/or other sources
        // detectFood(image)
        // Assert label texts and approximate confidence
    }



}